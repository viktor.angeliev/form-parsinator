import { FormComponentTextProps } from '../components/form-components/form-component-text/FormComponentText';
import { FormComponentDateProps } from '../components/form-components/form-component-date/FormComponentDate';
import { FormComponentCommandProps } from '../components/form-components/form-component-command/FormComponentCommand';

export type ViewComponentConfig =
	| FormComponentTextProps['config']
	| FormComponentDateProps['config']
	| FormComponentCommandProps['config'];

export type FormConfig = {
	view: {
		[key: string]: {
			title: string;
			children: { [key: string]: ViewComponentConfig };
		};
	};
	data: {
		[key: string]: {
			[key: string]: string;
		};
	};
};
