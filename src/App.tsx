import FormParser from './components/form-parser/FormParser';

function App() {
	return (
		<div className="app">
			<FormParser />
		</div>
	);
}

export default App;
