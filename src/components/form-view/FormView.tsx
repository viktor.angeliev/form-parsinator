import FormControl from '../form-control/FormControl';
import { ViewComponentConfig } from '../../services/formTypes';

type FormViewProps = {
	config: {
		title: string;
		children: {
			[key: string]: ViewComponentConfig;
		};
	};
	data: { [key: string]: any };
};

export default function FormView({ config, data }: FormViewProps) {
	return (
		<div>
			<h1>{config.title}</h1>
			{Object.entries(config.children).map(([key, value]) => (
				<FormControl key={key} config={value} data={data} />
			))}
		</div>
	);
}
