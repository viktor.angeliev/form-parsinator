export type FormComponentCommandProps = {
	config: {
		component: 'Command';
		text: string;
	};
};

export default function FormComponentCommand({ config }: FormComponentCommandProps) {
	return (
		<div>
			<button type="submit">{config.text}</button>
		</div>
	);
}
