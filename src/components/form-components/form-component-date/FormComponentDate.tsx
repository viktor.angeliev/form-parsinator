import { useMemo } from 'react';

export type FormComponentDateProps = {
	config: {
		component: 'Date';
		path: string;
		label: string;
		name: string;
	};
	data: { [key: string]: any };
};

export default function FormComponentDate({ config, data }: FormComponentDateProps) {
	//Parse date to proper format for field
	const value = useMemo(() => {
		try {
			const date = new Date(data[config.path]);
			return new Date(date.getTime() - date.getTimezoneOffset() * 60000).toISOString().slice(0, -1);
		} catch {
			return undefined;
		}
	}, [config, data]);

	return (
		<div>
			<span>{config.label}</span>
			<input type="datetime-local" name={config.name} defaultValue={value} />
		</div>
	);
}
