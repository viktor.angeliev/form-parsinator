export type FormComponentTextProps = {
	config: {
		component: 'Text';
		path: string;
		label: string;
		name: string;
	};
	data: { [key: string]: any };
};

export default function FormComponentText({ config, data }: FormComponentTextProps) {
	return (
		<div>
			<span>{config.label}</span>
			<input type="text" name={config.name} defaultValue={data[config.path]} />
		</div>
	);
}
