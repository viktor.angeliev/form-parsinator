import FormComponentDate from '../form-components/form-component-date/FormComponentDate';
import FormComponentText from '../form-components/form-component-text/FormComponentText';
import FormComponentCommand from '../form-components/form-component-command/FormComponentCommand';
import { ViewComponentConfig } from '../../services/formTypes';

type FormControlProps = {
	config: ViewComponentConfig;
	data: { [key: string]: any };
};

export default function FormControl({ config, data }: FormControlProps) {
	switch (config.component) {
		case 'Command':
			return <FormComponentCommand config={config} />;
		case 'Text':
			return <FormComponentText config={config} data={data} />;
		case 'Date':
			return <FormComponentDate config={config} data={data} />;
		default:
			return <p>Unknown component type</p>;
	}
}
